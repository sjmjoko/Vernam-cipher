#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include<bitset>
#include<vector>
using namespace std;
template < class T > 
void print(vector<T> v)
{
	for (int i = 0; i < v.size(); i++)
	{
		cout << v[i] << endl;
	}
	cout << endl;
}
static const char alphanum[] =
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz";

int stringLength = sizeof(alphanum) - 1;
void toBinary(vector<string> v, vector<int> w);
int main()
{
	char a;
	vector<char> plainText, cipherText, keyCode;           //plain text, cipher text and key vectors
	vector<int> arrPlain, arrCipher, arrKey,binCode;              //array of ASCII codes
	vector<bitset<8> > plainBin, cipherBin, keyBin;
	string plain;
	int ascii;
	cout << "Enter plain text: ";
	getline(cin, plain);                                   //takes the plain text from the user
	                                     
	int length = plain.length();
	//transfer the plain string to character plain text vector
	for (int i = 0; i < length; i++)
	{
		plainText.push_back(plain[i]);               
	}
	//srand(time(0));
	for (int i = 0; i < length; i++)
	{
		a = alphanum[rand() % stringLength];
		keyCode.push_back(a);
	}
	//convert plain text to ascii code
	for (int i = 0; i <length; i++)
	{
		ascii = int(plainText[i]);
		arrPlain.push_back(ascii);
	} 
	//convert key to ascii
	for (int i = 0; i <length; i++)
	{
		ascii = int(keyCode[i]);
		arrKey.push_back(ascii);
	}
	//convert text and key to binary
	//perform exclusive or
	for (int i = 0; i < 4; i++)
	{
		bitset<8> p(arrPlain[i]);     //plain text in bits
		bitset<8> k(arrKey[i]);       //key in bits
		bitset<8> c = (p ^= k);       //cipher text in bits
		cipherBin.push_back(a);
		cout << cipherBin[i] << endl;
	}
	cout << endl;
	system("pause");
	return 0;
}